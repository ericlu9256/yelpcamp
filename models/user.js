const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const passportLocalMongoose = require('passport-local-mongoose'); 


// username, and password fields will be created and hidden by plugging in passportLocalMongoose. 
const UserSchema = new Schema ({
    email: {
        type: String, 
        required: true, 
        unique: true
    }
})

// https://www.passportjs.org/docs/
UserSchema.plugin(passportLocalMongoose);



module.exports = mongoose.model('User', UserSchema); 