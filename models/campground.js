// Requiring Mongoose 
const mongoose = require('mongoose'); 
// Importing Review model. 
const Review = require('./review');
// Creat a variable for Schema -> for short cut
const Schema = mongoose.Schema; 

// Making our Schema 

const ImageSchema = new Schema({
    url: String, 
    filename: String
});
ImageSchema.virtual('thumbnail').get(function() {
    return this.url.replace('/upload', '/upload/w_200')
})


const opts = {toJSON: { virtuals: true } };
const CampgroundSchema = new Schema ({
    title: String,
    images: [ImageSchema], 
    geometry: { 
        type: {
            type: String, 
            enum: ['Point'], 
            required: true
        }, 
        coordinates: {
            type: [Number], 
            required: true
        }
    },
    price: Number, 
    description: String,
    location: String,
      // connecting with review Model, one to Many
    reviews: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Review'
        }
    ],
    author: 
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }

}, opts );

CampgroundSchema.virtual('properties.popUpMarkUp').get(function() {
    return `
    <strong><a href="/campgrounds/${this._id}">${this.title}</a></strong>
    <p>${this.description.substring(0, 30)} ...</p>`
})

// Middleware that handles deleting reviews when a campground is being deleted. 
// This Middleware only can be triggered when we are using ****.findByIdAndDelete()**** per mongoose documentation. 
CampgroundSchema.post('findOneAndDelete', async function (doc) {
    if (doc) {                                  // if doc exsits, 
        await Review.deleteMany({               // deleting reviews        
            _id: { $in: doc.reviews}            // review _ids in a specific campground's review [] 
        })
    }

}) 

// Exporting Schema -> model: Campground, Schema: CampgroundSchema 
module.exports = mongoose.model('Campground', CampgroundSchema); 
