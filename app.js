if (process.env.NODE_ENV !== "production") {
    require('dotenv').config();
}


const express = require('express'); 
// #######################################################################
// npm i method-override, this is for method "PUT" update campground.   ##
// #######################################################################
const methodOverride = require('method-override');
const path = require('path'); 
const port = process.env.PORT || 3000; 
// #######################################################################
// npm i joi,  for data validation, error handling, schema              ##
// #######################################################################
const Joi = require('joi'); // Importing Joi to do the error handling. 
const ejsMate = require('ejs-mate'); // requiring ejs-mate for EJS Tool for Layouts.
// #######################################################################
// npm i express-session,  for using session                            ##
// #######################################################################
const session = require('express-session'); 
// #######################################################################
// npm i connection-flash,  for showing msg based on session             ##
// #######################################################################
const flash = require('connect-flash');
const mongoose = require('mongoose'); 
const passport = require('passport');
const localStrategy = require('passport-local');
const User = require('./models/user');


const ExpressError = require('./utils/ExpressError') // Importing the ExpressError for error handling. 
const Review = require('./models/review.js') // Importing review  

const userRoutes = require('./routers/users.js');
const campgroundRoutes = require('./routers/campgrounds.js');
const reviewRoutes = require('./routers/reviews.js');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');

const MongoDBStore = require("connect-mongo")(session);

// process.env.DB_URL  
const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017/yelp-camp'   

// Changing mongoose to < 7.0.0 version to resolve passport compatibale issue, and adding this to get rid of error msg in terminal . 
mongoose.set('strictQuery', false);



// connecting mongoose, localhost port 27017 data collection: yelp-camp
mongoose.connect(dbUrl, {
    useNewUrlParser: true, 
    useunifiedTopology: true
});


// Error Handling for database connection. 
const db = mongoose.connection; 
db.on("error", console.error.bind(console, "connection error")); 
db.once("open", ()=>{
    console.log("Database Connected!");
});

const app = express(); 

app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'))
app.use(express.static(path.join(__dirname, 'public')))
app.use(mongoSanitize());
app.use(helmet());


const scriptSrcUrls = [
    "https://stackpath.bootstrapcdn.com/",
    "https://api.tiles.mapbox.com/",
    "https://api.mapbox.com/",
    "https://kit.fontawesome.com/",
    "https://cdnjs.cloudflare.com/",
    "https://cdn.jsdelivr.net",
];
const styleSrcUrls = [
    "https://kit-free.fontawesome.com/",
    "https://stackpath.bootstrapcdn.com/",
    "https://api.mapbox.com/",
    "https://api.tiles.mapbox.com/",
    "https://fonts.googleapis.com/",
    "https://use.fontawesome.com/",
];
const connectSrcUrls = [
    "https://api.mapbox.com/",
    "https://a.tiles.mapbox.com/",
    "https://b.tiles.mapbox.com/",
    "https://events.mapbox.com/",
];
const fontSrcUrls = [];
app.use(
    helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: [],
            connectSrc: ["'self'", ...connectSrcUrls],
            scriptSrc: ["'unsafe-inline'", "'self'", ...scriptSrcUrls],
            styleSrc: ["'self'", "'unsafe-inline'", ...styleSrcUrls],
            workerSrc: ["'self'", "blob:"],
            objectSrc: [],
            imgSrc: [
                "'self'",
                "blob:",
                "data:",
                "https://res.cloudinary.com/drkobb5cq/", //SHOULD MATCH YOUR CLOUDINARY ACCOUNT! 
                "https://images.unsplash.com/",
            ],
            fontSrc: ["'self'", ...fontSrcUrls],
        },
    })
);

const secret = process.env.SECRET || 'thisshouldbeabettersecret';

const store = new MongoDBStore({
    url: dbUrl, 
    secret,
    touchAfter: 24 * 60 * 60
});
store.on("error", function(e) {
    console.log("SESSION STORE ERROR")
})
const sessionConfig = {
    store,
    name:'_session', // Not to use default name. 
    secret,
    resave: false,
    saveUninitialized: true,
    cookie: {
        httpOnly: true, 
       // secure: true,  // https 
        expires: Date.now() + 1000 * 60 * 60 * 24 * 7, 
        maxAge: 1000 * 60 * 60 * 24 * 7
    }
}

app.use(session(sessionConfig)); 
app.use(flash());

// passport, localStrategy, and authentication Config
app.use(passport.initialize()); 
app.use(passport.session()); // be sure to use session() before passport.session(). checked. 
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());     // for logging in session
passport.deserializeUser(User.deserializeUser()); // for logging out session

// setting this before all route handlers. 
app.use((req, res, next )=> {
    res.locals.currentUser = req.user;    // passport: getting user id, name, and email. password, salt won't be shown.  -> navbar.ejs
    res.locals.success = req.flash('success'); 
    res.locals.error = req.flash('error'); 
    next(); // this is a middleware, make sure to call next() 
})



app.listen(port, () => {
    console.log(`LISTENING ON PORT:${port}`)
});

// Parse the body for POST
app.use(express.urlencoded ({extended: true}))
app.use(methodOverride('_method'));



// using the router: campgroundRoutes with pre `/campgrounds`
app.use('/campgrounds', campgroundRoutes); 
// using the router: reviewRoutes  with pre '/campgrounds/:id/reviews'
app.use('/campgrounds/:id/reviews', reviewRoutes);
// using the router : userRoutes. 
app.use('/', userRoutes);
// public 
app.use(express.static('public'))


// YelpCamp Homepage
app.get('/', (req, res) => {
    res.render('home')
});


// 404 catching. 
app.all('*', (req, res, next)=> {
    next(new ExpressError('Page Not Found', 404))
});

// Basic Error Handling. 
app.use((err, req, res, next)=> {
    const { statusCode = 500 }= err; 
    if(!err.message) err.message = 'Something went Wrong'
    res.status(statusCode).render('error', { err });
    // res.send("Something went WRONG! ")
});


