# YelpCamp

YelpCamp is a web application that allows users to discover and review campgrounds. Users can view campground listings, leave reviews, and add their own campgrounds. It's a platform designed for outdoor enthusiasts to share their camping experiences and find new destinations. served on heroku platform @ https://yelpcamp-lu.herokuapp.com/ 

![Screenshot_2023-06-01_at_3.40.50_PM](/uploads/fab1f5ee048aad9ff02ce733c2525949/Screenshot_2023-06-01_at_3.40.50_PM.png)

# Table of Contents
  [[_TOC_]]

## Features

- User authentication: Users can sign up, log in, and log out of their accounts.
- Campground management: Registered users can add new campgrounds, edit existing ones, and delete - campgrounds they own.
- Campground reviews: Users can leave reviews for campgrounds and read reviews left by others.
- Campground rating: Each campground is assigned an average rating based on user reviews.
- Image uploads: Users can upload multiple images of campgrounds along with their reviews.
- Flash messages: Provides feedback and notifications to users for various actions.

## Technologies Used

- Node.js
- Express.js
- MongoDB
- Mongoose
- Passport.js (for authentication)
- EJS (Embedded JavaScript) templating engine
- Bootstrap (front-end framework)
- Cloudinary (for image uploads)
- Mapbox (for campground location maps)

## Getting Started

To get started with YelpCamp, follow these steps:

1. Clone the repository: 

```
git clone https://gitlab.com/ericlu9256/yelpcamp.git 
```

2. install the dependencies: 

```
    cd yelpcamp 
    npm install
```

3. Set up the environment variables: 

- Rename the `.env.sample` file to `.env`
- Fill in the necessary environment variables like database connection URL, Cloudinary credentials, etc. 
```

CLOUDINARY_CLOUD_NAME=
CLOUDINARY_KEY=
CLOUDINARY_SECRET=
MAPBOX_TOKEN=
DB_URL=
```

4. Start the server: 

```
npm start
``` 

5. Open your browser and visit `http://localhost:3000` to view the yelpcamp application. 


## Folder Structure

- `app.js`: Entry point of the application.
- `controllers`: Contains configuration files and settings.
- `models`: Database models and schemas.
- `routes`: Express.js routes for handling different URL endpoints.
- `views`: EJS templates for rendering HTML pages.
- `public`: Static files such as CSS stylesheets, client-side JavaScript, and images.

## Acknowledgements

- The Web Developer Bootcamp by Colt Steele - Inspiration and guidance for building YelpCamp.


## Contact

For any inquiries or questions, you can reach out to me at ericlu9256@gmail.com.
