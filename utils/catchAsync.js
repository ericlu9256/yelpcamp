
// This module exports a higher-order function that acts as middleware in Express.js. 
// It executes the provided function asynchronously, handling any errors by passing them 
// to the next middleware or error handler.
module.exports = func => {
    return(req, res, next) => {
        func(req, res, next).catch(next);
    }
}