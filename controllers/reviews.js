const Review = require('../models/review.js') // Importing review  
const Campground = require('../models/campground'); //requiring models.


module.exports.createReview = async(req, res)=>{
    const campground = await Campground.findById(req.params.id);
    const review = new Review(req.body.review); 
    review.author = req.user._id;
    campground.reviews.push(review);    // adding the review under campground. 
    await review.save();                // saving to review collection
    await campground.save();            // saving to campground collection 
    req.flash('success', 'Successfully created a new review!'); 
    res.redirect(`/campgrounds/${campground._id}`) 
}

module.exports.deleteReview = async(req, res)=>{
    const {id, reviewId} = req.params;
    // uses the findByIdAndUpdate method from the Campground model to update a campground document
    // with the specified id. It utilizes the $pull operator to remove the specific reviewId 
    // from the reviews array within the campground document.
    await Campground.findByIdAndUpdate(id, { $pull: { reviews: reviewId }}); 
    await Review.findByIdAndDelete(reviewId); 
    req.flash('success', 'Successfully deleted review!'); 
    res.redirect(`/campgrounds/${id}`);
    
}