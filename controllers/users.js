const User = require('../models/user');


module.exports.renderUserRegister = ( req, res ) => {
    res.render('users/register');
}

module.exports.UserRegister = async ( req, res ) => {
    try {
        const { email, username, password } = req.body; 
        const user = new User({ email, username}); 
        const registerUser = await User.register(user, password);
        req.login(registerUser, err => {                    //this function require callback, so  no await avaliable; allowing user login when user successfully registered. 
            if(err) return next(err);
            req.flash('success', 'Welcome to YelpCamp!'); 
            res.redirect('/campgrounds')
        })
      
    } catch(e){
        req.flash('error', e.message);
        res.redirect('register');
    }
   
}

module.exports.renderUserLogin = ( req, res ) =>{
    res.render('users/login');
}


module.exports.userLogin = ( req, res ) =>{

    req.flash('success', `Hi ${req.body.username.toUpperCase()},  Welcome Back  to YelpCamp`);
    const redirectUrl = res.locals.returnTo || '/campgrounds' 
    res.redirect(redirectUrl);
}

module.exports.userLogout = (req, res, next)=>{
    req.logout(function (err) {
        if(err){
            return next(err);
        }
        req.flash('success', 'Goodbye!')
        res.redirect('/campgrounds')
    });
}