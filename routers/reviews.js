const express = require('express'); 
const router = express.Router({ mergeParams: true});  //to fix error:  Cannot read properties of null (reading 'reviews') 
const catchAsync = require('../utils/catchAsync')
const ExpressError = require('../utils/ExpressError') // Importing the ExpressError for error handling.
const Campground = require('../models/campground'); //requiring models.
const Review = require('../models/review.js') // Importing review  
const { validateReview, isLoggedIn, isReviewAuthor }  = require('../middleware');
const reviews = require('../controllers/reviews')



router.post('/', isLoggedIn, validateReview, catchAsync(reviews.createReview)) 

router.delete('/:reviewId', isLoggedIn, isReviewAuthor, catchAsync(reviews.deleteReview))


module.exports = router;