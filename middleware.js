const { campgroundSchema, reviewSchema} = require('./schema.js');
const Campground = require('./models/campground');
const ExpressError = require('./utils/ExpressError');
const Review = require('./models/review.js')




module.exports.isLoggedIn = (req, res, next ) => {
    if(!req.isAuthenticated()){
        req.session.returnTo = req.originalUrl; // add this line
        req.flash('error', 'You must be signed in first!');
        return res.redirect('/login');
    }
    next();
} 

module.exports.storeReturnTo = (req, res, next) => {
    if (req.session.returnTo) {
        res.locals.returnTo = req.session.returnTo;
    }
    next();
}


// isAuthor checking if the author who created the campground originally. 
module.exports.isAuthor = async (req, res, next) => {
    const { id } = req.params; 
    const campground = await Campground.findById(id); 
    if(!campground.author.equals(req.user._id)){
        req.flash('error', 'Sorry, You do not have permission to do that!'); 
        return res.redirect(`/campgrounds/${id}`)
    }
    next();
}


module.exports.isReviewAuthor = async (req, res, next) => {
    const { id, reviewId } = req.params; 
    const review = await Review.findById(reviewId); 
    if(!review.author.equals(req.user._id)){
        req.flash('error', 'Sorry, You do not have permission to do that!'); 
        return res.redirect(`/campgrounds/${id}`)
    }
    next();
}


module.exports.validateCampground = (req, res, next)=> {
    // =================================================================================================================
    // Error handling for user making POST request via Postman, etc.  Adding a layer for data validation on Server-side. 
    const { error } = campgroundSchema.validate(req.body);

    // Mapping the errorr into a single error string. Joining with ',' in case it contains more than one elements
    if(error){
        const msg = error.details.map(el=>el.message).join(','); 
        throw new ExpressError(msg, 400)
    } else{
        next();
    }
    // =================================================================================================================
}



module.exports.validateReview = (req, res, next) => {
    const { error } = reviewSchema.validate(req.body);
    if(error){
        const msg = error.details.map(el=>el.message).join(','); 
        throw new ExpressError(msg, 400)
    } else{
        next();
    }
}