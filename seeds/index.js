
// route changes .. back one more directory 
const Campground = require('../models/campground'); //requiring models
const cities = require('./cities');
const { places, descriptors} = require('./seedHelpers');
const mongoose = require('mongoose'); 

mongoose.set('strictQuery', false);
// connecting mongoose, localhost port 27017 data collection: yelp-camp
mongoose.connect('mongodb://localhost:27017/yelp-camp', {
    useNewUrlParser: true, 
    useunifiedTopology: true
});

// Error Handling for database connection. 
const db = mongoose.connection; 
db.on("error", console.error.bind(console, "connection error")); 
db.once("open", ()=>{
    console.log("Database Connected!");
});


const sample = (array) => array[Math.floor(Math.random() * array.length)]






// delete all the model and create a new one title with purple field. 
const seedDB = async ()=> {
    await Campground.deleteMany({}); 
    for (let i = 0; i < 50; i++){
        const random1000 = Math.floor(Math.random() * 1000); 
        const price = Math.floor(Math.random()* 150) + 10 ;
        const camp = new Campground({
            author: '6469b794a265db8dcd1d2a55',
            location:`${cities[random1000].city}, ${cities[random1000].state}`,
            title: `${sample(descriptors)} ${sample(places)}`,
            description: 'Nice Nice campground!',
            price: price,
            geometry:{
                type: "Point",
                coordinates: [
                    cities[random1000].longitude,
                    cities[random1000].latitude,
                ]
            },
            images: [
                {
                  url: 'https://res.cloudinary.com/drkobb5cq/image/upload/v1685071692/cld-sample-2.jpg',
                  filename: 'YelpCamp/mqlxoinbezobt9h2h0uu',
                }
              ]
        })
        await camp.save();
    }
   

}

//Excute the function 
seedDB().then(()=> {
    mongoose.connection.close()
});